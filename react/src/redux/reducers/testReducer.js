import { TESTING_ACTIONS } from "../actionType/test";

const initialState = {
    id: null,
};

export const test = (state = initialState, action) => {
    switch (action.type) {
        case "persist/REHYDRATE":
            return {
                ...state, persistedState: action.payload,
            };
        case TESTING_ACTIONS:
            return {
                ...state, id: action.payload
            };
        default:
            return state;
    }
};