import { combineReducers } from "redux";
import { reducer as reduxFormReducer } from "redux-form";
import { test } from '../reducers/testReducer';

const rootReducer = combineReducers({
    form: reduxFormReducer,
    test
});

export default rootReducer;