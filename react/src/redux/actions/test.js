import { TESTING_ACTIONS } from '../actionType/test';

function testing(payload) {
    return {
        type: TESTING_ACTIONS,
        payload
    }
}

export default {
    testing
}